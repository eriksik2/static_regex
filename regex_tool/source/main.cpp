#include <main.h>

#include <tstring.h>

int main() {

    auto name = MAKE_TSTRING("Erik");

    static_assert(name.size() == 4);
    static_assert(name.front() == 'E');
    static_assert(name.back() == 'k');
    static_assert(name.data()[1] == 'r');
    static_assert(name[2] == 'i');

    return 0;
}