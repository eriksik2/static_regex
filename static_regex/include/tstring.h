#pragma once

#include <iterator>
#include <string_view>

#include <constexpr_arg.h>

template<char...ch>
struct tstring {

    using size_type = size_t;
    using value_type = char;
    using const_reference = const value_type&;
    using const_pointer = const value_type*;
    using const_iterator = const_pointer;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    using reference = const_reference;
    using pointer = const_pointer;
    using iterator = const_iterator;
    using reverse_iterator = const_reverse_iterator;

    constexpr value_type operator[](size_type n) const { return data()[n]; }
    constexpr value_type at(size_type n) const { return operator[](n); }

    constexpr value_type front() const { return operator[](0); }
    constexpr value_type back() const { return operator[](size()-1); }

    constexpr const_pointer data() const { return m_data; }
    constexpr const_pointer c_str() const { return data(); }

    constexpr operator std::basic_string_view<value_type>() const { return std::basic_string_view<value_type>(data(), size()); }

    constexpr const_iterator begin() const { return data(); }
    constexpr const_iterator cbegin() const { return data(); }

    constexpr const_iterator end() const { return data() + size() + 1; }
    constexpr const_iterator cend() const { return data() + size() + 1; }

    constexpr const_reverse_iterator rbegin() const { return std::make_reverse_iterator(begin()); }
    constexpr const_reverse_iterator crbegin() const { return std::make_reverse_iterator(begin()); }

    constexpr const_reverse_iterator rend() const { return std::make_reverse_iterator(end()); }
    constexpr const_reverse_iterator crend() const { return std::make_reverse_iterator(end()); }

    constexpr bool empty() const { return size() == 0; }

    constexpr size_type size() const { return sizeof...(ch); }
    constexpr size_type length() const { return sizeof...(ch); }

    constexpr size_type max_size() const { return size_type(-1); }

private:
    constexpr static size_type m_size = sizeof...(ch);
    constexpr static value_type m_data[] = { ch... };
};

namespace std {
    template<size_t N, char...ch>
    constexpr char get(tstring<ch...>) {
        return std::get<N>(std::make_tuple(ch...));
    }
}

template<char...c1, char...c2>
constexpr auto operator+(tstring<c1...>, tstring<c2...>) {
    return tstring<c1..., c2...>{};
}


#if defined(_MSC_VER)
#define MAKE_TSTRING(...) make_tstring<static_cast<const char*(*)()>([]{ return (__VA_ARGS__); })>()
namespace make_tstring_help {
    template<const char* (*Tclass)(), int N, char...c>
    struct make_tstring {
        using type = typename make_tstring<Tclass, N-1, (Tclass)()[N-1], c...>::type;
    };
    template<const char* (*Tclass)(), char...c>
    struct make_tstring<Tclass, 0, c...> {
        using type = tstring<c...>;
    };
    constexpr size_t strlen(const char* str, size_t n = 0) {
        if(str[n] == 0) return n;
        return strlen(str, n+1);
    }
}
template<const char* (*Tclass)(), int Slen = make_tstring_help::strlen((Tclass)())>
using make_tstring = typename make_tstring_help::make_tstring<Tclass, Slen>::type;
#else
#define MAKE_TSTRING(...) make_tstring(CONSTEXPR_ARG(__VA_ARGS__))
template<class T> constexpr auto make_tstring(constexpr_arg<T> arg) {
    constexpr auto text = arg();

    if constexpr(text[0] == '\0')
        return tstring<'\0'>{};
    else
        return tstring<text[0]>() + MAKE_TSTRING(text+1);
}
#endif