#pragma once

namespace structure {
    template<class T>
    struct base {};

    // and: matches all its child elements in sequence
    template<class...T> struct sequence : base<sequence<T...>> {
        static constexpr bool internal_match(const char*& string) {
            // test all children in sequence on the same string.
            return (T::internal_match(string) && ...);
        }
    };
    template<> struct sequence<> : base<sequence<>> { // sequence specalization: sequence 0 elements always match
        static constexpr bool internal_match(const char*&) { return true; }
    };

    // or: matches one of its child elements
    template<class...T> struct one_of : base<one_of<T...>> {
        static constexpr bool internal_match(const char*& string) {
            const char* copy = string; // children are only called on a copy of string so they dont move it forward when they dont match
            auto match = (T::internal_match(copy = string) || ...); // test children from starting point until one matches
            if(match) string = copy; // if match was found, move string forward to end of match
            return match;
        }
    };
    template<> struct one_of<> : base<one_of<>> { // one_of specalization: picking between 0 elements always match
        static constexpr bool internal_match(const char*&) { return true; }
    };
    template<class T> struct one_of<T> : base<one_of<T>> { // one_of specalization: picking between 1 elements always match
        static constexpr bool internal_match(const char*&) { return true; }
    };

    // matches a sequence of min to max (inclusive) number of child element
    template<class T, size_t min, size_t max> struct count : base<count<T, min, max>> {
        static constexpr bool internal_match(const char*& string) {
            const char* copy = string; // child only called on copy
            for(size_t i = 0; i <= max; ++i) {
                auto match = T::internal_match(copy = string); // test on copy of string so it doesnt move fwd on nonmatch
                if(match) string = copy; // on match, move string forward for next iteration
                else return (i >= min);  // if child doesnt match, return true if we matched the required amnt of repetitions, false otherwise
            }
            return true; // if we get to the max repetitions without already returning, return true, its a match
        }
    };

    template<class T> using optional = count<T, 0, 1>; // optional: matches either 0 or 1 of child element
    template<class T> using any = count<T, 0, -1>; // any: matches any number of the child element in sequence
    template<class T> using multiple = count<T, 1, -1>; // any: matches at least 1 of the child element in sequence

    // matches one specific char (value)
    template<char ch> struct value : base<value<ch>> {
        static constexpr bool internal_match(const char*& string) {
            switch(*string) {
                default: return false; // do nothing but fail on no match
                case ch:
                    if constexpr(ch != '\0') ++string;
                    return true; // on match, move string forward if this is not end of string, then return true;
            }
        }
    };
}

