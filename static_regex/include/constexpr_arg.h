#pragma once

#include <type_traits>

template<class T, class ReturnType = decltype(std::declval<T>()())>
struct constexpr_arg {
    T lambda;

    constexpr constexpr_arg(T lambda) : lambda(lambda) {}

    constexpr ReturnType operator()() const { return lambda(); }
};

#define CONSTEXPR_ARG(...) (constexpr_arg([]{ return (__VA_ARGS__); }))